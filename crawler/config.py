HEADERS = {
    "User-Agent": "HazelBot/1.0 (+https://bitbucket.org/"
    "jadiego/hazel-exercise)",
}

BASE_URL = "https://ca.healthinspections.us/napa/"
START_URL = (
    BASE_URL + "search.cfm?start=1&1=1&sd=01/01/1970&ed=03/01/"
    "2017&kw1=&kw2=&kw3=&rel1=N.permitName&rel2=N.permitName&rel"
    "3=N.permitName&zc=&dtRng=YES&pre=similar"
)
REPORT_URL = (
    "https://ca.healthinspections.us/napa/"
    "estab.cfm?permitID=%s&lastThree=YES"
)

REPORT_URL_PATTERN = "report_full.cfm"
ESTABLISHMENT_URL_PATTERN = "estab.cfm"

CSS_SELECTOR = {
    "facilityName": ".topSection span:nth-of-type(1)",
    "facilityAddress": ".topSection span:nth-of-type(5)",
    "inspectionDate": ".topSection span:nth-of-type(3)",
    "inspectionType": ".topSection span:nth-of-type(10)",
    "inspectionGrade": ".page2Content table.totPtsTbl tr:nth-of-type(2) td:nth-of-type(2)",
    "inspectionRows": "table.mainTable tr td > table tr",
    "inspectionRowChecked": "td:nth-of-type(3) > img",
    "inspectionRowDescription": "td:nth-of-type(1)",
}
