from config import CSS_SELECTOR


class Report:
    """
    A helper class that includes methods for parsing reports from
    the Napa County health department website.
    """

    @staticmethod
    def parse(document):
        """
        Parses the report page, returning the report a dictionary. Collects:
            - Facility name
            - Street address
            - City
            - State
            - ZIP code
            - Inspection date
            - Inspection type
            - Inspection grade
            - Item number and description for each out-of-compliance violation

        Args:
            document (BeautifulSoup): A BeautifulSoup instance of the report
            page.
        """

        report = {}
        report["facilityName"] = Report._parse_facility_name(document)
        report["facilityAddress"] = Report._parse_address(document)
        report["inspection"] = Report._parse_inspection(document)
        report["violations"] = Report._parse_violations(document)

        return report

    @staticmethod
    def get_facility(report):
        """Returns given report's facility info as tuple"""
        return (
            report["facilityName"],
            report["facilityAddress"]["streetAddress"],
            report["facilityAddress"]["addressLocality"],
            report["facilityAddress"]["addressRegion"],
            report["facilityAddress"]["postalCode"]
        )

    def get_violation(report):
        """Returns the given report's violation as set"""
        violations = []
        for violation in report["violations"]:
            violations.append((violation["number"], violation["description"]))
        return violations

    @classmethod
    def _parse_violations(cls, document):
        violations = []
        """Returns a list of facility's out of compliance violations"""
        for tr in document.select(CSS_SELECTOR["inspectionRows"]):
            if (not tr.has_attr('class') and "box_checked" in
                    tr.select_one(
                    CSS_SELECTOR["inspectionRowChecked"])["src"]):
                description = tr.select_one(
                    CSS_SELECTOR["inspectionRowDescription"
                                 ]).get_text().split(".")
                violation = {
                    "number": description[0].strip(),
                    "description": description[1].strip(),
                }
                violations.append(violation)
        return violations

    @classmethod
    def _parse_facility_name(cls, document):
        """Return facility name."""
        return document.select_one(
            CSS_SELECTOR["facilityName"]).get_text()

    @classmethod
    def _parse_inspection(cls, document):
        """Return facility inspection info as dictionary."""
        result = {}
        result["date"] = document.select_one(
            CSS_SELECTOR["inspectionDate"]).get_text()
        result["type"] = document.select_one(
            CSS_SELECTOR["inspectionType"]).get_text()
        result["grade"] = document.select_one(
            CSS_SELECTOR["inspectionGrade"]).get_text()
        result["grade"] = result["grade"].replace("\xa0", "")
        return result

    @classmethod
    def _parse_address(cls, document):
        """
        Splits the address into street address, state, and zip code.
        Returns it as a dictionary.
        """
        address = document.select_one(
            CSS_SELECTOR["facilityAddress"]).get_text()

        result = {}
        address = address.split("\r")
        result["streetAddress"] = address[0].strip().upper()
        result["addressLocality"] = address[1].split(",")[0].strip().upper()
        result["addressRegion"] = address[1].split(",")[1].strip()[:2]
        result["postalCode"] = address[1].strip()[-5:]

        return result
