import sqlite3
from os import path

from report import Report


class ReportsDB():
    """
    Database for storing and querying the collected reports from
    crawling the the Napa County health department website.
    """

    def __init__(self, name="reports.db"):
        self.db = sqlite3.connect(name)
        self._setup()

    def _setup(self):
        """
        Setup database with 4 Tables
        (Facilities, Violations, ReportViolations, Reports)
        if not already existing.
        """
        with open(path.join(path.dirname(__file__), "schema.sqlite")) as fp:
            self.db.executescript(fp.read())

    def close(self):
        """Close connection with database."""
        self.db.close()

    def _insert_facilities(self, reports):
        """
        Checks if given facilities is already within database.
        If so, updates it. If not, inserts it.
        """
        query = self.db.cursor()
        for report in reports:
            # check if facility already exists
            id = self._get_facility_id(query, report["facilityName"])
            facility = Report.get_facility(report)
            # if it doesn't, insert into database
            if id is None:
                query.execute(
                    "INSERT INTO facilities"
                    "(name, address, city, state, zip)"
                    "VALUES (?, ?, ?, ?, ?)",
                    facility
                )
        query.close()
        self.db.commit()

    def _insert_violations(self, reports):
        """
        Inserts the violations into the database, if non existent
        """
        violations = list(map(Report.get_violation, reports))
        total_violations = []
        for v1 in violations:
            for v2 in v1:
                total_violations.append(v2)
        self.db.executemany(
            "INSERT OR IGNORE INTO violations"
            "(number, description)"
            "VALUES (?, ?)",
            total_violations
        )
        self.db.commit()

    def _insert_inspections(self, reports):
        """
        Inserts the inspections into the database, if non existent
        """
        query = self.db.cursor()
        for report in reports:
            id = self._get_facility_id(query, report["facilityName"])
            query.execute(
                "INSERT OR REPLACE INTO reports"
                "(facility_id, date, inspection_type, inspection_grade)"
                "VALUES (?, ?, ?, ?)",
                (id, report["inspection"]["date"],
                    report["inspection"]["type"],
                    report["inspection"]["grade"])
            )
        query.close()
        self.db.commit()

    def _insert_reportviolations(self, reports):
        """
        Inserts report violations for the given reports into
        the database.
        """
        query = self.db.cursor()
        for report in reports:
            fac_id = self._get_facility_id(query, report["facilityName"])
            report_id = self._get_report_id(
                query, fac_id, report["inspection"]["date"],
                report["inspection"]["type"], report["inspection"]["grade"])
            query.execute(
                "INSERT OR REPLACE INTO reportviolations"
                "(report_id, violation_id)"
                "VALUES (?, ?)",
                (fac_id, report_id)
            )
        query.close()
        self.db.commit()

    def _get_facility_id(self, query, name):
        row = query.execute(
            "SELECT id FROM facilities WHERE name == ?",
            (name,)
        )
        row = query.fetchone()
        if row is None:
            return None
        else:
            return row[0]

    def _get_report_id(self, query, facilityid, date, itype, grade):
        row = query.execute(
            "SELECT id FROM reports WHERE facility_id == ?"
            "AND date == ? AND inspection_type == ?"
            "and inspection_grade == ?",
            (facilityid, date, itype, grade)
        )
        row = query.fetchone()
        if row is None:
            return None
        else:
            return row[0]

    def push(self, reports):
        """
        Given a single report, updates the necessary tables with new
        report information.
        """
        # Must be done in sequence below
        self._insert_facilities(reports)
        self._insert_violations(reports)
        self._insert_inspections(reports)
        self._insert_reportviolations(reports)
