import json
import re
import sys
import urllib.parse as urlp
from queue import Queue
from threading import RLock, Thread

import requests as req
from bs4 import BeautifulSoup as bsoup

import config as conf
from db import ReportsDB
from report import Report


class Crawler:
    """
    A web crawler for the Napa County health department website. This crawler
    searches the website for links to inspection report for each facility
    found.
    """

    def __init__(self, maxWorkers=20):
        self.report_links = Queue(maxWorkers * 2)
        self.finished_reports = []
        self.maxWorkers = maxWorkers
        self.lock = RLock()
        self.db = ReportsDB()

    def crawl(self, log=True):
        # collect links to crawl
        print("Collecting report links...")
        self.get_inspection_report_links()

        # parse reports from links
        print("Parsing inspection information for each report...", end="",
              flush=True)
        for i in range(self.maxWorkers):
            t = Thread(target=self._parse_report)
            t.daemon = True
            t.start()
        self.report_links.join()
        print(" done.\n")

        # store collected information to disk
        print("Storing results to database...", end="", flush=True)
        self.db.push(self.finished_reports)
        self.db.close()
        print(" done.")

        # log reports
        if log:
            print(json.dumps(self.finished_reports,
                             separators=(',', ':'), indent=4))

    def get_inspection_report_links(self):
        """
        Return a list of links of facility inspection reports.
        """
        html = self._get_dom(conf.START_URL)
        # begin with result page and load page for each establishment
        for establishment_link in html.find_all(
                href=re.compile(conf.ESTABLISHMENT_URL_PATTERN)):
            link = conf.BASE_URL + establishment_link["href"]
            # parse the permit ID of establishment
            permitid = urlp.parse_qs(
                urlp.urlparse(link).query)["permitID"][0]
            # load page that includes previous inspections
            # for establishment
            html = self._get_dom(conf.REPORT_URL % permitid)
            # search and collect report page links
            for link in html.find_all(
                    href=re.compile(conf.REPORT_URL_PATTERN)):
                report_link = conf.BASE_URL + link["href"]
                self.report_links.put(report_link.replace(" ", "%20"))

    def _parse_report(self):
        """
        Analyzes the inspection report from the given link, and collects
        specific report information.
        """
        while True:
            url = self.report_links.get()
            document = self._get_dom(url)

            report = Report.parse(document)
            self.lock.acquire()
            self.finished_reports.append(report)
            self.lock.release()

            self.report_links.task_done()

    def _get_dom(self, url):
        """Return the page content for the given url"""
        try:
            page = req.get(url, headers=conf.HEADERS)
            return bsoup(page.content, "lxml")
        except req.exceptions.RequestException as err:
            print("\tFailed request to: %s" % url)
            sys.exit(1)


if __name__ == "__main__":
    from time import time

    start_time = time()
    c = Crawler()
    c.crawl()

    print("Done in %.2f seconds" % (time() - start_time))
