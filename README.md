# hazel-exercise

This repo is for the Hazel Analytics coding exercise: intern/part-time.

It contains a python web crawler module for crawling the 
Napa County health department website. It collects certain
information from inspection reports of a number of facilities.

The script stores the results in a local SQLite database, and also
prints the results as a JSON.

## Getting Started

Install dependencies
```bash
# If pipenv is installed
pipenv install
# If pip is installed
pip install -r requirements.txt
```

Run crawler and print results after collecting results
```bash
pipenv run python crawler/crawler.py
```

## Task
Your task is to write a Python program to do the following:

  1) Access the first page of results for health inspection information from the Napa County health department website.

  2) Access each inspection of each facility on that page and collect the following information:
     - Facility name
     - Street address
     - City
     - State
     - ZIP code
     - Inspection date
     - Inspection type
     - Inspection grade
     - Item number and description for each out-of-compliance violation

     * For example, violation item number 6 has the description "Adequate handwashing facilities supplied & accessible"

  3) Create a JSON structure to store the collected information, and write that JSON structure to disk.

  4) Print the collected information to the console in an easy-to-understand format.

  5) _[optional bonus task]_ Create a SQL schema and store the collected information in it.

Some starter code has been provided, but you are welcome to organize your code in the manner that you prefer.

We encourage you to use one of the following libraries to process the reports:
-  LXML (https://pypi.org/project/lxml/)
-  BeautifulSoup (https://pypi.org/project/beautifulsoup4/)
